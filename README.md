# bbb-ffmpeg-exporter
Big blue button renderer based on ffmpeg

This script is inspired by [BBB render](https://github.com/plugorgau/bbb-render) but is much more faster than GES renderer.

The script is generating all frames one by one and merge them into one video with sound from `webcams.webm`. Video manipulations done by `ffmpeg`.

Features :
- [x] Slides
- [x] Deskshare
- [ ] Webcams
- [ ] Chat
- [ ] Whiteboard

## Requirements

ffmpeg

## Usage

```
python script.py <url>
```
